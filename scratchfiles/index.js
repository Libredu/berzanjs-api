const fetch = require("node-fetch");

const proxy = "https://cors-anywhere.herokuapp.com/";
const className = "Te19E";
const weekDay = 0;
const week = 2;

fetch(proxy + "https://web.skola24.se/timetable/timetable-viewer/data/render", {
    method: "POST",
    headers: {
        "Content-Type": "application/json",
        "X-Scope": "8a22163c-8662-4535-9050-bc5e1923df48",
        "X-Requested-With": "XMLHttpRequest"
    },
    body: JSON.stringify({
        "divWidth": 1920,
        "divHeight": 1050,
        "domain": "linkoping.skola24.se",
        "headerEnabled": "false",
        "selectedClass": {
            "guid": "Zjg2NTE3NjAtOTFhMi1mMDQ4LWFmNmMtNzI4OGMxNGMzMTEx",
            "id": className.toUpperCase(),
            "isClass": true
        },
        "selectedCourse": null,
        "selectedDay": weekDay,
        "selectedGroup": null,
        "selectedPeriod": null,
        "selectedRoom": null,
        "selectedSignatures": null,
        "selectedStudent": null,
        "selectedSubject": null,
        "selectedTeacher": null,
        "selectedUnit": {
            "isTopUnit": false,
            "name": "Berzeliusskolan gymnasium",
            "schoolGuid": null,
            "settings": {
                "activateViewer": true,
                "allowCalendarExport": false
            },
            "unitGuid": "ODUzZGRmNmMtYzdiNy1mZTA3LThlMTctNzIyNDY2Mjk1Y2I2",
        },
        "selectedWeek": week
    })
}).then(response => {
    return response.text();
}).then(response_text => {
    console.log(response_text);
});

