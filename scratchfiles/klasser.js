const fetch = require("node-fetch");

const proxy = "https://cors-anywhere.herokuapp.com/";
const query = `https://web.skola24.se/timetable/timetable-viewer/data/selection?` +
    `hostName=linkoping.skola24.se&` +
    `unitGuid=ODUzZGRmNmMtYzdiNy1mZTA3LThlMTctNzIyNDY2Mjk1Y2I2&` +
    `encryptedText=&` +
    `_=${Date.now()}`;

fetch(proxy + query, {
    "credentials": "include",
    "headers": {
        "User-Agent": "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0",
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "Accept-Language": "sv,en-GB;q=0.7,en;q=0.3",
        "Content-Type": "application/json",
        "X-Scope": "8a22163c-8662-4535-9050-bc5e1923df48",
        "X-Requested-With": "XMLHttpRequest",
        "Pragma": "no-cache",
        "Cache-Control": "no-cache"
    },
    "referrer": "https://web.skola24.se/timetable/timetable-viewer/linkoping.skola24.se/Berzeliusskolan%20gymnasium",
    "method": "GET",
    "mode": "cors"
}).then(response => {
    return response.text();
}).then(response_text => {
    console.log(response_text);
});

