const fetch = require("node-fetch");
const fs = require("fs");
const http = require("http");

const urlParserRegex = /\?(.+)/;

const port = process.env.PORT || 8080;
const host = process.env.HOST || undefined;

http.createServer(function(request, response) {
    console.log("Request ", request.url);
    console.log(parseGET(request.url));

    const query = getQueryPart(request.url);
    const GETParams = parseGET(request.url);

    switch (query) {
        case "/klasser":
            const query = `https://web.skola24.se/timetable/timetable-viewer/data/selection?` +
                `hostName=linkoping.skola24.se&` +
                `unitGuid=ODUzZGRmNmMtYzdiNy1mZTA3LThlMTctNzIyNDY2Mjk1Y2I2&` +
                `encryptedText=&` +
                `_=${Date.now()}`;
            fetch(query, {
                "credentials": "include",
                "headers": {
                    "User-Agent": "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0",
                    "Accept": "application/json, text/javascript, */*; q=0.01",
                    "Accept-Language": "sv,en-GB;q=0.7,en;q=0.3",
                    "Content-Type": "application/json",
                    "X-Scope": "8a22163c-8662-4535-9050-bc5e1923df48",
                    "X-Requested-With": "XMLHttpRequest",
                    "Pragma": "no-cache",
                    "Cache-Control": "no-cache"
                },
                "referrer": "https://web.skola24.se/timetable/timetable-viewer/linkoping.skola24.se/Berzeliusskolan%20gymnasium",
                "method": "GET",
                "mode": "cors"
            }).then(classesResponse => {
                return classesResponse.text();
            }).then(classesJSON => {
                response.writeHead(200, {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                });
                response.end(classesJSON, "utf-8");
            });
            break;
        case "/schema":
            const week = GETParams.get("week");
            const weekDay = GETParams.get("week-day");
            const className = GETParams.get("class-name");
            const classGUID = GETParams.get("class-guid");
            const width = GETParams.get("width");
            const height = GETParams.get("height");

            if (
                typeof week === "string" &&
                typeof weekDay === "string" &&
                typeof className === "string" &&
                typeof classGUID === "string" &&
                typeof width === "string" &&
                typeof height === "string"
            ) {
                fetch("https://web.skola24.se/timetable/timetable-viewer/data/render", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "X-Scope": "8a22163c-8662-4535-9050-bc5e1923df48",
                        "X-Requested-With": "XMLHttpRequest"
                    },
                    body: JSON.stringify({
                        "divWidth": width,
                        "divHeight": height,
                        "domain": "linkoping.skola24.se",
                        "headerEnabled": "false",
                        "selectedClass": {
                            "guid": classGUID,
                            "id": className.toUpperCase(),
                            "isClass": true
                        },
                        "selectedCourse": null,
                        "selectedDay": weekDay,
                        "selectedGroup": null,
                        "selectedPeriod": null,
                        "selectedRoom": null,
                        "selectedSignatures": null,
                        "selectedStudent": null,
                        "selectedSubject": null,
                        "selectedTeacher": null,
                        "selectedUnit": {
                            "isTopUnit": false,
                            "name": "Berzeliusskolan gymnasium",
                            "schoolGuid": null,
                            "settings": {
                                "activateViewer": true,
                                "allowCalendarExport": false
                            },
                            "unitGuid": "ODUzZGRmNmMtYzdiNy1mZTA3LThlMTctNzIyNDY2Mjk1Y2I2",
                        },
                        "selectedWeek": week
                    })
                }).then(scheduleResponse => {
                    return scheduleResponse.text();
                }).then(scheduleResponseText => {
                    endResponse200(response, scheduleResponseText, "application/json");
                });
            } else {
                endResponse400(response);
            }
            break;
        default:
            showIndex(response);
            break;
    }
}).listen(port, host);

console.log(`Listening on port ${port}`);

function showIndex(response) {
    fs.readFile("./html/index.html", function(error, content) {
        if (error === null) {
            endResponse200(response, content, "text/html")
        } else {
            endResponse500(
                response,
                "<p>Something went wrong when loading the index file" + error + "</p>",
                "text/html"
            )
        }
    });
}

function writeHead(response, statusCode, contentType) {
    response.writeHead(statusCode, {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": contentType
    });
}

function endResponse500(response, contents, contentType) {
    writeHead(response, 500, contentType);
    response.end(contents, "utf-8");
}

function endResponse400(response) {
    fs.readFile("./html/400.html", function(error, content) {
        if (error === null) {
            writeHead(response, 400, "text/html");
            response.end(content, "utf-8");
        } else {
            endResponse500(
                response,
                "<p>Something went wrong when reading the file. Here's what: " + error + "</p>",
                "text/html"
            );
        }
    });
}

function endResponse200(response, contents, contentType) {
    writeHead(response, 200, contentType);

    response.end(contents, "utf-8");
}

function parseGET(url) {
    const result = url.split(urlParserRegex);

    if (result.length > 1) {
        const params = result[1];
        const pairs = params.split("&");
        const keyValueMap = new Map();

        for (let i = 0; i < pairs.length; i++) {
            const keyValue = pairs[i].split("=");

            keyValueMap.set(
                keyValue[0],
                keyValue[1]
            );
        }

        return keyValueMap;
    } else {
        return new Map();
    }
}

function getQueryPart(url) {
    return url.split(urlParserRegex)[0];
}