# Note: Deprecated in favour of Rust rewrite: https://gitlab.com/libredu/eduprox

If someone is interested in updating this to work with the new API schema, feel
 free to send a merge request, I'll be happy to review it and likely merge it,
 but I do not plan to work on this further myself. 

# The Berzan.js API Proxy

## What's this?

Once upon a time, [Berzan.js](https://github.com/Newbytee/berzan) could get away with only being a frontend for the then called Novasoftware schedule API. As Berzeliusskolan (Berzan) eventually switched over to Skola24, the schedule provider's new API, this was no longer the case due to how CORS works.

This is an API that proxies requests in order to bypass the CORS restrictions so that Berzan.js can utilise the new API.

## How to use

Despite this being built for Berzan.js, anyone can use this for their own Skola24 frontends! Information about how is coming soon™.

## Setting up and running yourself

### System software requirements

Running the Berzan.js API Proxy yourself requires [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/). The code was originally written for Node.js 12, the current LTS version at the time of writing, but will most likely work well on later versions too.

### Setting up

Start by running `npm install` to fetch all dependencies. The code only requires an implementation of `fetch()`. I choose node-fetch, but it could most likely be switched out for something else.

After that, you're done!

### Running the server

Running the server is done via the `npm start` command. If you want it to run on a different port than 8080, set the PORT environment variable, like so: `PORT=8084 npm start`

## License

The Berzan.js API Proxy is licensed under the Mozilla Public License, version 2.0. A copy of it should have been provided with the source code, but in the situation this was not the case, you can access the license here:  https://www.mozilla.org/en-US/MPL/2.0/
